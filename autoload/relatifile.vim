" relatifile:
" A set of helper functions to deal with relative file paths in Vim.
" BSD 3-clause, (c) 2021 Tim Weber
" <https://codeberg.org/scy/relatifile>

function! relatifile#relative(names, dir)
	let names = a:names
	let listarg = 1
	if type(a:names) == v:t_string
		let names = [a:names]
		let listarg = 0
	endif
	let cmd = 'realpath --relative-to ' . shellescape(a:dir)
	for name in names
		let cmd .= ' ' . shellescape(name)
	endfor
	silent let result = systemlist(cmd)
	if listarg
		return result
	endif
	return result[0]
endfunction

function! relatifile#relative_to_current(names)
	return relatifile#relative(a:names, expand('%:h'))
endfunction

function! relatifile#relative_to_cwd(names)
	return relatifile#relative(na:ames, getcwd())
endfunction

function! relatifile#relativize_under_cursor()
	let file = expand('<cfile>:p')
	if file == ''
		return
	endif
	let @/ = '\m\f*\%#\f*'
	execute 'normal! gnc' . relatifile#relative_to_current(file)
endfunction
