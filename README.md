# relatifile

A set of helper functions to deal with relative file paths in Vim.

## What it does

It can convert a file path (probably an _absolute_ file path) to a path relative to a given directory.

## How it does that

Since Vim [apparently doesn’t have such a function](https://vi.stackexchange.com/q/28835/33217) and I didn’t want to write one myself, _relatifile_ simply calls the [`realpath` utility](https://www.gnu.org/software/coreutils/manual/html_node/realpath-invocation.html) included in GNU Coreutils to do the heavy lifting.

This also means that if you don’t have `realpath` available, or if it doesn’t support the `--relative-to` argument, _relatifile_ won’t work.

Note that there is proof-of-concept code that gets rid of the `realpath` dependency available in [#1](https://codeberg.org/scy/relatifile/issues/1), it’s just not implemented or tested yet.
I’m probably to busy right now and `realpath` works for me, but I’m open to pull requests.

## Functions provided

### `relatifile#relative(names, dir)`

Convert `names` (can be either a string or a list of strings) to be relative to `dir`.

If `names` is a string, the value returned is also a string.
If `names` is a list, a list is returned.
This allows you to convert multiple names with a single `realpath` invocation.

### `relatifile#relative_to_current(names)`

Shortcut for `relatifile#relative(names, expand('%:h'))`, i.e. make the `names` relative to the current file.

### `relatifile#relative_to_cwd(names)`

Shortcut for `relatifile#relative(names, getcwd())`, i.e. make the `names` relative to the current working directory.

### `relatifile#relativize_under_cursor()`

Convert the file path under the cursor to one that’s relative to the current file.

This will do a `relative_to_current` conversion for `expand('<cfile>:p')`, then select the file under the cursor by setting `@/` to `\m\f*\%#\f*` followed by `gn`, then replace it with the result.

## Known bugs

File names with non-printable characters, especially file names with newlines in them, will probably confuse _relatifile_.

## Status

_relatifile_ is maintained by Tim Weber.
Its official home is at <https://codeberg.org/scy/relatifile>.

Currently, I consider this thing feature-complete.
I might add additional things to scratch an itch.

Feel free to open issues for bugs and feature requests.
